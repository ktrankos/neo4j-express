var neo4jApp = angular.module('neo4jApp', []);
neo4jApp.controller('UserListCtrl', function ($scope, $compile, $element, $http) 
{
	$scope.list = [];
	var _el = $($element);

	_el.find('tbody tr').each(function(){
		$scope.list.push({
			_id: $(this).find('td:eq(0)').text(),
			name: $(this).find('td:eq(1)').text(),
			from: $(this).find('td:eq(2)').text(),
			age: $(this).find('td:eq(3)').text(),
		});
	});
	console.log($scope.list);


	$scope.edit=function(e, id)
	{
		e.preventDefault();
		var _tr = $(e.currentTarget).parent().parent();
		var _id = id;
		
		

		var _name = _tr.find('td:eq(1)');
		var _nameVal = _name.text();
		

		var _from = _tr.find('td:eq(2)');
		var _fromVal = _from.text();
		

		var _age = _tr.find('td:eq(3)');
		var _ageVal = _age.text();


		var _iAge = '<input type="text" name="age" value="'+_ageVal+'"  ng-enter="put('+_tr.index()+')"/>';
		_age.html(
			$compile(_iAge)($scope)
		);
		var _iName = '<input type="text" name="name" value="'+_nameVal+'" ng-enter="put('+_tr.index()+')" />';		
		_name.html(
			$compile(_iName)($scope)
		);
		var _iFrom = '<input type="text" name="from" value="'+_fromVal+'"  ng-enter="put('+_tr.index()+')" />';
		_from.html(
			$compile(_iFrom)($scope)
		);
		//$scope.$apply();		
	}
	$scope.$watch('list', function(newValue, oldValue){
		//console.log(newValue, oldValue);
	});
	$scope.put = function(id)
	{

		var _tr = _el.find('tbody tr:eq("'+id+'")');
		var _name = _tr.find('input[type="text"]:eq(0)').val();
		var _from = _tr.find('input[type="text"]:eq(1)').val();
		var _age = _tr.find('input[type="text"]:eq(2)').val();
		$scope.list[id].name = _name;
		$scope.list[id].from = _from;
		$scope.list[id].age = _age;
		$http.put('/edit', 	$scope.list[id])
		.success(function(data, status, headers, config) {
		    // this callback will be called asynchronously
		    // when the response is available
		  })
		.error(function(data, status, headers, config) {
		    // this callback will be called asynchronously
		    // when the response is available
		  });

		console.log(_tr, _name, _from, _age, $scope.list[id] );

	}
});
/*neo4jApp.directive('atsEdit', ['$interval', 'dateFilter', function($interval, dateFilter) {

  function link(scope, element, attrs) 
  {
  	element.bind('click', function(e){
  		console.log("editr");
  	});

    scope.$watch(attrs.myCurrentTime, function(value) {
      
    });

    element.on('$destroy', function() {
      
    });

    // start the UI update process; save the timeoutId for canceling
    
  }

  return {
    link: link
  };
}]);*/
neo4jApp.directive('ngEnter', function() {
	return function(scope, element, attrs) {
		element.bind("keydown keypress", function(event) {
			if(event.which === 13) {
				scope.$apply(function(){
					scope.$eval(attrs.ngEnter);
				});
                        
				event.preventDefault();
			}
		});
	};
});
